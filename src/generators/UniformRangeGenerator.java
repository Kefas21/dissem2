/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generators;

import java.util.Random;

/**
 *
 * @author peter
 */
public class UniformRangeGenerator {
    private double min;
    private double max;
    private Random generator;

    /**
     *
     * @param seed
     * @param min
     * @param max
     */
    public UniformRangeGenerator(long seed, double min, double max) {
        this.min = min;
        this.max = max;
        this.generator=new Random(seed);
    }
    
    /**
     *
     * @return
     */
    public double getNextValue(){
        return (generator.nextDouble()*(max-min))+min;
    } 
}
