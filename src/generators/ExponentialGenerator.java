/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generators;

import java.util.Random;

/**
 *
 * @author peter
 */
public class ExponentialGenerator {
    private Random rand;
    private double lambda;
    
    /**
     *
     * @param seed
     * @param lambda
     */
    public ExponentialGenerator(long seed, double lambda){
        this.rand=new Random(seed);
        this.lambda=lambda;
    }
    
    /**
     *
     * @return
     */
    public double getNextValue(){
        return Math.log(1-rand.nextDouble())/(-1/lambda);
    }

    /**
     *
     * @return
     */
    public Random getRand() {
        return rand;
    }

    /**
     *
     * @param rand
     */
    public void setRand(Random rand) {
        this.rand = rand;
    }
    
}
