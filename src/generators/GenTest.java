/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generators;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;
import rental.Globals;

/**
 *
 * @author peter
 */
public class GenTest {
    
    /**
     *
     * @param args
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException{
        Random r = new Random();
        ExponentialGenerator eg = new ExponentialGenerator(r.nextLong(), Globals.T1_ARRIVAL);       
        PrintWriter writer = new PrintWriter("testfile.txt");

        for(int i=0;i<10000;i++){
            double n=eg.getNextValue();
            writer.println(n);
            System.out.println(i+". : "+n);
        }
        writer.close();
    }
    
}
