/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dissem2;

import rental.RentalCore;

/**
 *
 * @author peter
 */
public interface ISimDelegate {

    /**
     *
     * @param core
     */
    void refresh(RentalCore core);
}
