/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dissem2;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import rental.EventMessage;
import rental.Globals;
import rental.RentalCore;

/**
 * Main GUI class
 * @author peter
 */
public class DISsem2 extends Application implements ISimDelegate {

    private RentalCore rc = new RentalCore();
    private LineChart<Number, Number> busLinechart;
    private LineChart<Number, Number> serviceLinechart;
    private XYChart.Series<Number, Number> busSeries;
    private XYChart.Series<Number, Number> serviceSeries;
    private ObservableList<EventMessage> tableData;
    private long sleepTime;
    private boolean tick;
    private int obsBus;
    private int obsEmp;
    private Label simCount;
    private Label repCount;
    private Button startBtn;
    private Button pauseBtn;
    private Button stopBtn;
    private Label simStateLabel;
    private Label timeLabel;
    private Label q1Label;
    private Label q1TimeLabel;
    private Label q2TimeLabel;
    private Label q3TimeLabel;
    private Label q2Label;
    private Label q3Label;
    private Label BEcounter;
    private Label avgTimeLabel;
    private Label rightIS;
    private Label leftIS;
    private TableView infoTable;

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("AirCarRental");
        primaryStage.setWidth(1700);
        primaryStage.setHeight(800);
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_LEFT);
        grid.setHgap(50);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        //Sim options
        Label optsLabel = new Label("Simulation options");
        grid.add(optsLabel, 0, 0);

        Label minBusNumberLabel = new Label("Min number of buses:");
        grid.add(minBusNumberLabel, 0, 1);
        TextField minBusNumberField = new TextField();
        minBusNumberField.setText("5");
        minBusNumberField.setMaxWidth(30);
        grid.add(minBusNumberField, 1, 1);

        Label maxBusNumberLabel = new Label("Max number of buses:");
        grid.add(maxBusNumberLabel, 0, 2);
        TextField maxBusNumberField = new TextField();
        maxBusNumberField.setText("20");
        maxBusNumberField.setMaxWidth(30);
        grid.add(maxBusNumberField, 1, 2);

        Label minSerNumberLabel = new Label("Min number of employees:");
        grid.add(minSerNumberLabel, 0, 3);
        TextField minSerNumberField = new TextField();
        minSerNumberField.setText("5");
        minSerNumberField.setMaxWidth(30);
        grid.add(minSerNumberField, 1, 3);

        Label maxSerNumberLabel = new Label("Min number of employees:");
        grid.add(maxSerNumberLabel, 0, 4);
        TextField maxSerNumberField = new TextField();
        maxSerNumberField.setText("20");
        maxSerNumberField.setMaxWidth(30);
        grid.add(maxSerNumberField, 1, 4);

        //Observation options
        Label obsLabel = new Label("Observe options");
        grid.add(obsLabel, 2, 0);

        Label obsBusNumberLabel = new Label("Bus number to observe:");
        grid.add(obsBusNumberLabel, 2, 1);
        TextField obsBusNumberField = new TextField();
        obsBusNumberField.setText("6");
        obsBusNumberField.setMaxWidth(30);
        grid.add(obsBusNumberField, 3, 1);

        Label obsEmpNumberLabel = new Label("Employee number to observe:");
        grid.add(obsEmpNumberLabel, 2, 2);
        TextField obsEmpNumberField = new TextField();
        obsEmpNumberField.setText("8");
        obsEmpNumberField.setMaxWidth(30);
        grid.add(obsEmpNumberField, 3, 2);

        //Sim speed options
        Label speedLabel = new Label("Simulation speed");
        grid.add(speedLabel, 4, 0);
        ToggleGroup speedGroup = new ToggleGroup();
        RadioButton normalSpeedRadio = new RadioButton("Fast");
        normalSpeedRadio.setSelected(true);
        normalSpeedRadio.setToggleGroup(speedGroup);
        grid.add(normalSpeedRadio, 4, 1);
        RadioButton slowSpeedRadio = new RadioButton("Slow");
        slowSpeedRadio.setToggleGroup(speedGroup);
        grid.add(slowSpeedRadio, 4, 2);
        RadioButton superslowSpeedRadio = new RadioButton("Super Slow");
        superslowSpeedRadio.setToggleGroup(speedGroup);
        grid.add(superslowSpeedRadio, 4, 3);

        //Sim start/pause/stop options
        Label actionLabel = new Label("Actions");
        grid.add(actionLabel, 5, 0);
        startBtn = new Button("Start");
        pauseBtn = new Button("Pause");
        stopBtn = new Button("Stop");
        startBtn.setMinWidth(100);
        pauseBtn.setMinWidth(100);
        stopBtn.setMinWidth(100);
        pauseBtn.setDisable(true);
        stopBtn.setDisable(true);
        grid.add(startBtn, 5, 1);
        grid.add(pauseBtn, 5, 2);
        grid.add(stopBtn, 5, 3);

        //Sim tick on/off
        Label tickLabel = new Label("Tick");
        grid.add(tickLabel, 6, 0);
        ToggleGroup tickGroup = new ToggleGroup();
        RadioButton tickOnRadio = new RadioButton("Tick ON");
        tickOnRadio.setToggleGroup(tickGroup);
        tickOnRadio.setDisable(true);
        grid.add(tickOnRadio, 6, 1);
        RadioButton tickOffRadio = new RadioButton("Tick OFF");
        tickOffRadio.setSelected(true);
        tickOffRadio.setDisable(true);
        tickOffRadio.setToggleGroup(tickGroup);
        grid.add(tickOffRadio, 6, 2);

        tickGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
            if (tickGroup.getSelectedToggle() == tickOnRadio) {
                this.tick = true;
                rc.setTick(true);
            }
            if (tickGroup.getSelectedToggle() == tickOffRadio) {
                this.tick = false;
                rc.setTick(false);
            }
        });
        
        
        speedGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) -> {
            if (speedGroup.getSelectedToggle() == slowSpeedRadio) {
                this.sleepTime = 500;
                rc.setSleepTime((long) 500);
                tickOffRadio.setDisable(false);
                tickOnRadio.setDisable(false);
            }
            if (speedGroup.getSelectedToggle() == normalSpeedRadio) {
                this.sleepTime = 0;
                rc.setSleepTime((long) 0);
                tickOffRadio.setSelected(true);
                tickOffRadio.setDisable(true);
                tickOnRadio.setDisable(true);
            }
            if (speedGroup.getSelectedToggle() == superslowSpeedRadio) {
                this.sleepTime = 1000;
                rc.setSleepTime((long) 1000);
                tickOffRadio.setDisable(false);
                tickOnRadio.setDisable(false);
            }
        });

        //Sim time info
        Label simInfoLabel = new Label("Simulation Info");
        grid.add(simInfoLabel, 7, 0);
        Label simLabel = new Label("Simulation:");
        simCount = new Label();
        grid.add(simLabel, 7, 1);
        grid.add(simCount, 8, 1);
        Label repLabel = new Label("Replication:");
        repCount = new Label();
        grid.add(repLabel, 7, 2);
        grid.add(repCount, 8, 2);
        Label simState = new Label("Simulation state:");
        simStateLabel = new Label();
        grid.add(simState, 7, 3);
        grid.add(simStateLabel, 8, 3);
        Label tLabel = new Label("Replication time:");
        timeLabel = new Label();
        grid.add(tLabel, 7, 4);
        grid.add(timeLabel, 8, 4);
        
        //Queue stats
        Label avgInfoLabel = new Label("Average Queue Statistics");
        grid.add(avgInfoLabel,0,6);
        Label q1InfoLabel = new Label("Average queue T1:");
        grid.add(q1InfoLabel,0,7);
        q1Label = new Label();
        grid.add(q1Label,1,7);
        Label q2InfoLabel = new Label("Average queue T2:");
        grid.add(q2InfoLabel,0,8);
        q2Label = new Label();
        grid.add(q2Label,1,8);
        Label q3InfoLabel = new Label("Average queue Rental:");
        grid.add(q3InfoLabel,0,9);
        q3Label = new Label();
        grid.add(q3Label,1,9);
        
        //queue time stats
        Label timeInfoLabel = new Label("Average Queue Time Statistics");
        grid.add(timeInfoLabel, 2, 6);
        Label q1tInfoLabel = new Label("Average time in queue T1:");
        grid.add(q1tInfoLabel,2,7);
        q1TimeLabel = new Label();
        grid.add(q1TimeLabel,3,7);
        Label q2tInfoLabel = new Label("Average time in queue T2:");
        grid.add(q2tInfoLabel,2,8);
        q2TimeLabel = new Label();
        grid.add(q2TimeLabel,3,8);
        Label q3tInfoLabel = new Label("Average time in queue Rental:");
        grid.add(q3tInfoLabel,2,9);
        q3TimeLabel = new Label();
        grid.add(q3TimeLabel,3,9);
        
        //system time stats
        Label systemInfoLabel = new Label("Average System Time Statistics");
        grid.add(systemInfoLabel, 4, 6);
        Label counterLabel = new Label("Buses/Employees:");
        grid.add(counterLabel,4,7);
        BEcounter = new Label();
        grid.add(BEcounter,5,7);
        Label avgLabel = new Label("Average time in system (min):");
        grid.add(avgLabel,4,8);
        avgTimeLabel = new Label();
        grid.add(avgTimeLabel,5,8);
        Label leftISLabel = new Label("Confidence interval left (min):");
        grid.add(leftISLabel,4,9);
        leftIS = new Label();
        grid.add(leftIS,5,9);
        Label rightISLabel = new Label("Confidence interval right (min):");
        grid.add(rightISLabel,4,10);
        rightIS = new Label();
        grid.add(rightIS,5,10);

        //charts
        setUpLineChart();
        grid.add(busLinechart, 0, 5, 4, 1);
        grid.add(serviceLinechart, 4, 5, 4, 1);

        //event table
        infoTable = new TableView();
        TableColumn timeCol = new TableColumn("Time");
        TableColumn eventCol = new TableColumn("Event");
        timeCol.setCellValueFactory(new PropertyValueFactory<>("time"));
        eventCol.setCellValueFactory(new PropertyValueFactory<>("message"));
        timeCol.setMinWidth(100);
        eventCol.setMinWidth(150);

        grid.add(infoTable, 8, 5, 3, 1);
        tableData = FXCollections.observableArrayList();
        infoTable.setItems(tableData);
        infoTable.getColumns().addAll(timeCol, eventCol);

        startBtn.setOnAction((ActionEvent event) -> {
            if (rc.isPaused() && rc.isStart()) {
                rc.setPaused(false);
                startBtn.setDisable(true);
                pauseBtn.setDisable(false);
                stopBtn.setDisable(false);
                simStateLabel.setText("Running");
            } else {
                int minB = Integer.parseInt(minBusNumberField.getText());
                int maxB = Integer.parseInt(maxBusNumberField.getText());
                int minE = Integer.parseInt(minSerNumberField.getText());
                int maxE = Integer.parseInt(maxSerNumberField.getText());
                obsBus = Integer.parseInt(obsBusNumberField.getText());
                obsEmp = Integer.parseInt(obsEmpNumberField.getText());
                if (minB <= maxB && minE <= maxE) {
                    startBtn.setDisable(true);
                    pauseBtn.setDisable(false);
                    stopBtn.setDisable(false);
                    rc = new RentalCore(Globals.REPLICATION_TIME, 100, tick, sleepTime, minB, maxB, minE, maxE);
                    rc.registerDelegate(this);
                    clearCharts();
                    tableData.clear();
                    simStateLabel.setText("Running");
                    rc.simulateAsync();
                }
            }
        });

        pauseBtn.setOnAction((ActionEvent event) -> {
            rc.setPaused(true);
            startBtn.setDisable(false);
            pauseBtn.setDisable(true);
            simStateLabel.setText("Paused");
        });

        stopBtn.setOnAction((ActionEvent event) -> {
            rc.setStart(false);
            rc.setStopped(true);
            startBtn.setDisable(false);
            stopBtn.setDisable(true);
            pauseBtn.setDisable(true);
            simStateLabel.setText("Stopped");
        });

        Scene scene = new Scene(grid);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    private void setUpLineChart() {
        //set up chart
        NumberAxis xbAxis = new NumberAxis();
        xbAxis.setLabel("Time (min)");
        xbAxis.setAutoRanging(true);
        NumberAxis ybAxis = new NumberAxis();
        ybAxis.setLabel("Employees");
        ybAxis.setAutoRanging(true);
        ybAxis.forceZeroInRangeProperty().set(false);

        NumberAxis xsAxis = new NumberAxis();
        xsAxis.setLabel("Time (min)");
        xsAxis.setAutoRanging(true);
        NumberAxis ysAxis = new NumberAxis();
        ysAxis.setLabel("Buses");
        ysAxis.setAutoRanging(true);
        ysAxis.forceZeroInRangeProperty().set(false);

        busLinechart = new LineChart<>(xbAxis, ybAxis);
        serviceLinechart = new LineChart<>(xsAxis, ysAxis);
        busLinechart.setCreateSymbols(true);
        serviceLinechart.setCreateSymbols(true);
        this.busSeries = new XYChart.Series<>();
        this.serviceSeries = new XYChart.Series<>();
        busSeries.setName("Average Time");
        serviceSeries.setName("Average Time");

        busLinechart.getData().add(busSeries);
        serviceLinechart.getData().add(serviceSeries);

    }

    /**
     * Clear all chart data
     */
    public void clearCharts() {
        busSeries.getData().clear();
        serviceSeries.getData().clear();
    }

    /**
     * Observer pattern, refresh method
     * @param core
     */
    @Override
    public void refresh(RentalCore core) {
        //charts refresh
        EventMessage hlpEm;
        if (core.getTotal() != null) {
            if (core.getSimCounter() == core.getTotal().getSimNr()) {
                int bus = core.getTotal().getBusNumber();
                int empl = core.getTotal().getEmployeeNumber();
                Platform.runLater(() -> {
                    if (bus == obsBus) {
                        busSeries.getData().add(new XYChart.Data<>(core.getTotal().getAvgTime(), core.getTotal().getEmployeeNumber()));
                    }
                    if (empl == obsEmp) {
                        serviceSeries.getData().add(new XYChart.Data<>(core.getTotal().getAvgTime(), core.getTotal().getBusNumber()));
                    }
                });
            }
        }
        //stats refresh
        Platform.runLater(() -> {
            q1TimeLabel.setText(TimeConverter.onlyTimeConverter(core.getQueueT1().getAvgTime()));
            q2TimeLabel.setText(TimeConverter.onlyTimeConverter(core.getQueueT2().getAvgTime()));
            q3TimeLabel.setText(TimeConverter.onlyTimeConverter(core.getQueueRental().getAvgTime()));
            q1Label.setText(String.valueOf(core.getQueueLT1().getAvgLength()));
            q2Label.setText(String.valueOf(core.getQueueLT2().getAvgLength()));
            q3Label.setText(String.valueOf(core.getQueueLR().getAvgLength()));
            //System.out.println(String.valueOf(core.getQueueLR().getAvgLength()));
            BEcounter.setText(core.getBusNumber()+"/"+core.getEmployeeNumber());
            timeLabel.setText(TimeConverter.converter(core.getExecTime()));
            simCount.setText(core.getSimCounter() + "/" + core.getSimTotal());
            repCount.setText(core.getActualRep() + "/" + core.getReplications());
            if (core.getSimCounter() == core.getSimTotal()
                    && core.getActualRep() == core.getReplications()) {
                startBtn.setDisable(false);
                pauseBtn.setDisable(true);
                stopBtn.setDisable(true);
                simStateLabel.setText("Done");
            }
        });
        //event table refresh
        if (core.getEm()!=null && !core.isStopped()){
            hlpEm = core.getEm();
        Platform.runLater(() -> {            
            tableData.add(hlpEm);
            infoTable.scrollTo(tableData.size() - 1);
        });
        }
        //avg time stats refresh
        if (core.getCumulStat()!=null){
        Platform.runLater(() -> {            
            avgTimeLabel.setText(TimeConverter.onlyTimeConverter(core.getCumulStat().getAvgTime()*60));
            leftIS.setText(TimeConverter.onlyTimeConverter(core.getCumulStat().getLeft()*60));
            rightIS.setText(TimeConverter.onlyTimeConverter(core.getCumulStat().getRight()*60));
        });
        }
    }
}
