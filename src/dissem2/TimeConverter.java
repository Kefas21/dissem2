/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dissem2;

/**
 *
 * @author peter
 */
public class TimeConverter {

    /**
     *
     * @param time
     * @return
     */
    public static String converter(double time) {
        String secs;
        String mins;
        String hours;
        String days;
        double hlpTime;
        //if (time == 0) return ""; //"Day 0, 00:00:00";
        days = Integer.toString((int) time / (24 * 60 * 60));
        hlpTime = time % (24 * 60 * 60);
        hours = Integer.toString((int) hlpTime / (60 * 60));
        hlpTime = hlpTime % (60 * 60);
        mins = Integer.toString((int) hlpTime / 60);
        hlpTime = hlpTime % 60;
        secs = Integer.toString((int) hlpTime);
        return "Day " + days + " , " + hours + ":" + mins + ":" + secs;
    }
    
    /**
     *
     * @param time
     * @return
     */
    public static String onlyTimeConverter(double time) {
        String secs;
        String mins;
        String hours;
        double hlpTime;
        hours = Integer.toString((int) time / (60 * 60));
        hlpTime = time % (60 * 60);
        mins = Integer.toString((int) hlpTime / 60);
        hlpTime = hlpTime % 60;
        secs = Integer.toString((int) hlpTime);
        return hours + ":" + mins + ":" + secs;
    }
}
