/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dissem2;

import rental.Globals;
import rental.RentalCore;

/**
 *
 * @author peter
 */
public class main {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        RentalCore rc = new RentalCore(Globals.REPLICATION_TIME, 1000, false, 0, 11, 11, 15, 15);
        rc.simulateSync();
    }
}
