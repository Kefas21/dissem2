/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dissem2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author peter
 */
public class ExcelWork {

    private String FILE_NAME = "SimulationResults.xlsx";
    private Map<String, Object[]> data;

    /**
     *
     */
    public ExcelWork() {
        data = new TreeMap<String, Object[]>();
    }

    /**
     *
     * @throws InterruptedException
     */
    public void write() throws InterruptedException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Sim Data");
        int rowNum = 0;
        Set<String> keyset = data.keySet();
        for (String key : keyset) {
            //create a row of excelsheet
            Row row = sheet.createRow(rowNum++);

            //get object array of prerticuler key
            Object[] objArr = data.get(key);

            int cellnum = 0;

            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }
        try {
            try ( //Write the workbook in file system
                    FileOutputStream out = new FileOutputStream(new File(FILE_NAME))) {
                workbook.write(out);
            }
            System.out.println(".xlsx written successfully on disk.");
        } catch (IOException e) {
        }
    }

    /**
     *
     * @return
     */
    public String getFILE_NAME() {
        return FILE_NAME;
    }

    /**
     *
     * @param FILE_NAME
     */
    public void setFILE_NAME(String FILE_NAME) {
        this.FILE_NAME = FILE_NAME;
    }

    /**
     *
     * @return
     */
    public Map<String, Object[]> getData() {
        return data;
    }

    /**
     *
     * @param data
     */
    public void setData(Map<String, Object[]> data) {
        this.data = data;
    }

}
