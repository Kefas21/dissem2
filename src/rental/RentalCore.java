/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental;

import statistics.Stat;
import core.SimulationCore;
import dissem2.ISimDelegate;
import events.AbstractRentalEvent;
import events.ArrivalT1;
import events.ArrivalT2;
import events.InputStartT1;
import events.InputStartT2;
import events.OutputStartRental;
import events.Tick;
import generators.ExponentialGenerator;
import generators.UniformRangeGenerator;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import statistics.StatInQueue;
import statistics.StatQueue;

/**
 * Implementation of core, all logic is here
 *
 * @author peter
 */
public class RentalCore extends SimulationCore {

    private Random seedGen;
    private ExponentialGenerator terminal1ArrivalGen;
    private ExponentialGenerator terminal2ArrivalGen;
    private UniformRangeGenerator serviceGen;
    private UniformRangeGenerator inputGen;
    private UniformRangeGenerator outputGen;
    private Queue<Customer> terminal1;
    private Queue<Customer> terminal2;
    private Queue<Customer> rentalService;
    private int busNumber;
    private int employeeNumber;
    private int minBusNumber;
    private int minEmployeeNumber;
    private int maxBusNumber;
    private int maxEmployeeNumber;
    private List<Employee> employees;
    private double timeCounter;
    private int customerCounter;
    private Stat cumulStat;
    private Stat total;
    private double interval;
    private boolean tick;
    private int simCounter = 0;
    private int simTotal;
    private ArrayList<ISimDelegate> delegates;
    private double execTime = 0;
    private EventMessage em;
    private StatInQueue queueT1;
    private StatInQueue queueT2;
    private StatInQueue queueRental;
    private StatQueue queueLT1;
    private StatQueue queueLT2;
    private StatQueue queueLR;
    private Map excel;

    /**
     *
     * @param max
     * @param reps
     * @param tick
     * @param sleepTime
     * @param minB
     * @param maxB
     * @param minE
     * @param maxE
     */
    public RentalCore(double max, int reps, boolean tick, long sleepTime,
            int minB, int maxB, int minE, int maxE) {
        super(max, reps, sleepTime);
        this.tick = tick;
        this.minBusNumber = minB;
        this.maxBusNumber = maxB;
        this.minEmployeeNumber = minE;
        this.maxEmployeeNumber = maxE;
        this.delegates = new ArrayList<>();
        int bCount = maxBusNumber - minBusNumber + 1;
        int eCount = maxEmployeeNumber - minEmployeeNumber + 1;
        this.simTotal = bCount * eCount;
        this.queueT1 = new StatInQueue();
        this.queueT2 = new StatInQueue();
        this.queueRental = new StatInQueue();
        this.queueLT1 = new StatQueue();
        this.queueLT2 = new StatQueue();
        this.queueLR = new StatQueue();
        this.excel = new TreeMap<String, Object[]>();
    }

    /**
     *
     */
    public RentalCore() {
        super(Globals.REPLICATION_TIME, 100);
        tick = false;
    }

    @Override
    public void beforeSimulation() {
        timeCounter = 0;
        customerCounter = 0;
        interval = 0;
        terminal1 = new LinkedList<>();
        terminal2 = new LinkedList<>();
        rentalService = new LinkedList<>();
        employees = new ArrayList<>();
        simCounter++;
        queueT1.clear();
        queueT2.clear();
        queueRental.clear();
        queueLT1.clear();
        queueLT2.clear();
        queueLR.clear();
        refreshGUI();
        seedGen = new Random();
        terminal1ArrivalGen = new ExponentialGenerator(seedGen.nextLong(), Globals.T1_ARRIVAL);
        terminal2ArrivalGen = new ExponentialGenerator(seedGen.nextLong(), Globals.T2_ARRIVAL);
        serviceGen = new UniformRangeGenerator(seedGen.nextLong(), Globals.SERVICE_MIN, Globals.SERVICE_MAX);
        inputGen = new UniformRangeGenerator(seedGen.nextLong(), Globals.CUSTOMER_INPUT_MIN, Globals.CUSTOMER_INPUT_MAX);
        outputGen = new UniformRangeGenerator(seedGen.nextLong(), Globals.CUSTOMER_OUTPUT_MIN, Globals.CUSTOMER_OUTPUT_MAX);
    }

    /**
     *
     */
    @Override
    public void beforeReplication() {
        terminal1.clear();
        terminal2.clear();
        rentalService.clear();
        employees.clear();
        execTime = 0;
        refreshGUI();
        super.getTimeline().clear();
        super.setCurrentTime(0);
        if (tick) {
            insertEvent(new Tick(0));
        }
        insertEvent(new ArrivalT1(new Customer(terminal1ArrivalGen.getNextValue())));
        insertEvent(new ArrivalT2(new Customer(terminal2ArrivalGen.getNextValue())));

        for (int i = 0; i < busNumber; i++) {
            if (i % 3 == 0) {
                insertEvent(new InputStartT1(new Minibus(Globals.BUS_CAPACITY), Globals.TIME_RENTAL_T1));
            } else if (i % 3 == 1) {
                insertEvent(new InputStartT2(new Minibus(Globals.BUS_CAPACITY), Globals.TIME_T1_T2));
            } else if (i % 3 == 2) {
                insertEvent(new OutputStartRental(new Minibus(Globals.BUS_CAPACITY), Globals.TIME_T2_RENTAL));
            }
        }
        for (int j = 0; j < employeeNumber; j++) {
            employees.add(new Employee());
        }
    }

    /**
     * Only for Excel output
     *
     * @throws InterruptedException
     */
    public void simulateSync() throws InterruptedException {
        int hlp = 0;
        for (int i = minBusNumber; i <= maxBusNumber; i++) {//busy
            if (super.isStopped()) {
                break;
            }
            for (int j = minEmployeeNumber; j <= maxEmployeeNumber; j++) {//obsluha
                if (super.isStopped()) {
                    break;
                }
                System.out.println("Busy: " + i + " Obsluha: " + j);
                busNumber = i;
                employeeNumber = j;
                simulation();
                Object[] obj = new Object[]{total.getBusNumber(), total.getEmployeeNumber(), String.valueOf(total.getAvgTime()), String.valueOf(total.getLeft()), String.valueOf(total.getRight())};
                excel.put(String.valueOf(hlp), obj);
                hlp++;
            }
        }
        XSSFWorkbook workbook = new XSSFWorkbook();

        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");

        //Iterate over data and write to sheet
        Set<String> keyset = excel.keySet();

        int rownum = 0;
        for (String key : keyset) {
            //create a row of excelsheet
            Row row = sheet.createRow(rownum++);

            //get object array of prerticuler key
            Object[] objArr = (Object[]) excel.get(key);

            int cellnum = 0;

            for (Object obj : objArr) {
                Cell cell = row.createCell(cellnum++);
                if (obj instanceof String) {
                    cell.setCellValue((String) obj);
                } else if (obj instanceof Integer) {
                    cell.setCellValue((Integer) obj);
                }
            }
        }
        try {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("results.xlsx"));
            workbook.write(out);
            out.close();
            System.out.println(".xlsx written successfully on disk.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Simulatiing in thread
     */
    public void simulateAsync() {
        Thread thread = new Thread(() -> {
            try {
                for (int i = minBusNumber; i <= maxBusNumber; i++) {//busy
                    if (super.isStopped()) {
                        break;
                    }
                    for (int j = minEmployeeNumber; j <= maxEmployeeNumber; j++) {//obsluha
                        if (super.isStopped()) {
                            break;
                        }
                        System.out.println("Busy: " + i + " Obsluha: " + j);
                        busNumber = i;
                        employeeNumber = j;
                        simulation();
                    }
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(RentalCore.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        thread.setName("SimThread");
        thread.setDaemon(true);
        thread.setPriority(Thread.NORM_PRIORITY);
        thread.start();
    }

    /**
     *
     * @throws InterruptedException
     */
    @Override
    public void afterReplication() throws InterruptedException {
        //System.out.println("Replication nr. " + super.getActualRep() + " DONE!");
        super.afterReplication();
        cumulStat = new Stat(customerCounter, timeCounter, interval, busNumber, employeeNumber, simCounter);
        refreshGUI();
    }

    @Override
    public void afterSimulation() {
        super.afterSimulation();
        total = new Stat(customerCounter, timeCounter, interval, busNumber, employeeNumber, simCounter);
        refreshGUI();
        System.out.println("Priemerny cas: " + total.getAvgTime() + " lavy: " + total.getLeft() + " pravy: " + total.getRight());
    }

    /**
     *
     * @return
     */
    public boolean isEmployeeAvailable() {
        for (Employee e : employees) {
            if (e.isFree()) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @return
     */
    public Employee employeeAvailable() {
        for (Employee e : employees) {
            if (e.isFree()) {
                return e;
            }
        }
        return null;
    }

    /**
     *
     * @param e
     */
    public void insertEvent(AbstractRentalEvent e) {
        e.setRentCore(this);
        super.insertEvent(e);
    }

    /**
     *
     * @param delegate
     */
    public void registerDelegate(ISimDelegate delegate) {
        delegates.add(delegate);
    }

    /**
     *
     */
    public void refreshGUI() {
        for (ISimDelegate d : delegates) {
            d.refresh(this);
        }
    }

    /**
     *
     * @return
     */
    public Random getSeedGen() {
        return seedGen;
    }

    /**
     *
     * @param seedGen
     */
    public void setSeedGen(Random seedGen) {
        this.seedGen = seedGen;
    }

    /**
     *
     * @return
     */
    public ExponentialGenerator getTerminal1ArrivalGen() {
        return terminal1ArrivalGen;
    }

    /**
     *
     * @param terminal1ArrivalGen
     */
    public void setTerminal1ArrivalGen(ExponentialGenerator terminal1ArrivalGen) {
        this.terminal1ArrivalGen = terminal1ArrivalGen;
    }

    /**
     *
     * @return
     */
    public ExponentialGenerator getTerminal2ArrivalGen() {
        return terminal2ArrivalGen;
    }

    /**
     *
     * @param terminal2ArrivalGen
     */
    public void setTerminal2ArrivalGen(ExponentialGenerator terminal2ArrivalGen) {
        this.terminal2ArrivalGen = terminal2ArrivalGen;
    }

    /**
     *
     * @return
     */
    public UniformRangeGenerator getServiceGen() {
        return serviceGen;
    }

    /**
     *
     * @param serviceGen
     */
    public void setServiceGen(UniformRangeGenerator serviceGen) {
        this.serviceGen = serviceGen;
    }

    /**
     *
     * @return
     */
    public UniformRangeGenerator getInputGen() {
        return inputGen;
    }

    /**
     *
     * @param inputGen
     */
    public void setInputGen(UniformRangeGenerator inputGen) {
        this.inputGen = inputGen;
    }

    /**
     *
     * @return
     */
    public UniformRangeGenerator getOutputGen() {
        return outputGen;
    }

    /**
     *
     * @param outputGen
     */
    public void setOutputGen(UniformRangeGenerator outputGen) {
        this.outputGen = outputGen;
    }

    /**
     *
     * @return
     */
    public Queue<Customer> getTerminal1() {
        return terminal1;
    }

    /**
     *
     * @param terminal1
     */
    public void setTerminal1(Queue<Customer> terminal1) {
        this.terminal1 = terminal1;
    }

    /**
     *
     * @return
     */
    public Queue<Customer> getTerminal2() {
        return terminal2;
    }

    /**
     *
     * @param terminal2
     */
    public void setTerminal2(Queue<Customer> terminal2) {
        this.terminal2 = terminal2;
    }

    /**
     *
     * @return
     */
    public Queue<Customer> getRentalService() {
        return rentalService;
    }

    /**
     *
     * @param rentalService
     */
    public void setRentalService(Queue<Customer> rentalService) {
        this.rentalService = rentalService;
    }

    /**
     *
     * @return
     */
    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     *
     * @param employees
     */
    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    /**
     *
     * @return
     */
    public int getBusNumber() {
        return busNumber;
    }

    /**
     *
     * @param busNumber
     */
    public void setBusNumber(int busNumber) {
        this.busNumber = busNumber;
    }

    /**
     *
     * @return
     */
    public int getEmployeeNumber() {
        return employeeNumber;
    }

    /**
     *
     * @param employeeNumber
     */
    public void setEmployeeNumber(int employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    /**
     *
     * @return
     */
    public double getTimeCounter() {
        return timeCounter;
    }

    /**
     *
     * @param timeCounter
     */
    public void setTimeCounter(double timeCounter) {
        this.timeCounter = timeCounter;
    }

    /**
     *
     * @return
     */
    public int getCustomerCounter() {
        return customerCounter;
    }

    /**
     *
     * @param customerCounter
     */
    public void setCustomerCounter(int customerCounter) {
        this.customerCounter = customerCounter;
    }

    /**
     *
     * @return
     */
    public Stat getTotal() {
        return total;
    }

    /**
     *
     * @param total
     */
    public void setTotal(Stat total) {
        this.total = total;
    }

    /**
     *
     * @return
     */
    public double getInterval() {
        return interval;
    }

    /**
     *
     * @param interval
     */
    public void setInterval(double interval) {
        this.interval = interval;
    }

    /**
     *
     * @return
     */
    public boolean isTick() {
        return tick;
    }

    /**
     *
     * @param tick
     */
    public void setTick(boolean tick) {
        this.tick = tick;
    }

    /**
     *
     * @return
     */
    public int getMinBusNumber() {
        return minBusNumber;
    }

    /**
     *
     * @param minBusNumber
     */
    public void setMinBusNumber(int minBusNumber) {
        this.minBusNumber = minBusNumber;
    }

    /**
     *
     * @return
     */
    public int getMinEmployeeNumber() {
        return minEmployeeNumber;
    }

    /**
     *
     * @param minEmployeeNumber
     */
    public void setMinEmployeeNumber(int minEmployeeNumber) {
        this.minEmployeeNumber = minEmployeeNumber;
    }

    /**
     *
     * @return
     */
    public int getMaxBusNumber() {
        return maxBusNumber;
    }

    /**
     *
     * @param maxBusNumber
     */
    public void setMaxBusNumber(int maxBusNumber) {
        this.maxBusNumber = maxBusNumber;
    }

    /**
     *
     * @return
     */
    public int getMaxEmployeeNumber() {
        return maxEmployeeNumber;
    }

    /**
     *
     * @param maxEmployeeNumber
     */
    public void setMaxEmployeeNumber(int maxEmployeeNumber) {
        this.maxEmployeeNumber = maxEmployeeNumber;
    }

    /**
     *
     * @return
     */
    public int getSimCounter() {
        return simCounter;
    }

    /**
     *
     * @param simCounter
     */
    public void setSimCounter(int simCounter) {
        this.simCounter = simCounter;
    }

    /**
     *
     * @return
     */
    public int getSimTotal() {
        return simTotal;
    }

    /**
     *
     * @param simTotal
     */
    public void setSimTotal(int simTotal) {
        this.simTotal = simTotal;
    }

    /**
     *
     * @return
     */
    public ArrayList<ISimDelegate> getDelegates() {
        return delegates;
    }

    /**
     *
     * @param delegates
     */
    public void setDelegates(ArrayList<ISimDelegate> delegates) {
        this.delegates = delegates;
    }

    /**
     *
     * @return
     */
    public double getExecTime() {
        return execTime;
    }

    /**
     *
     * @param execTime
     */
    public void setExecTime(double execTime) {
        this.execTime = execTime;
    }

    /**
     *
     * @return
     */
    public EventMessage getEm() {
        return em;
    }

    /**
     *
     * @param em
     */
    public void setEm(EventMessage em) {
        this.em = em;
    }

    /**
     *
     * @return
     */
    public Stat getCumulStat() {
        return cumulStat;
    }

    /**
     *
     * @param cumulStat
     */
    public void setCumulStat(Stat cumulStat) {
        this.cumulStat = cumulStat;
    }

    /**
     *
     * @return
     */
    public StatInQueue getQueueT1() {
        return queueT1;
    }

    /**
     *
     * @param queueT1
     */
    public void setQueueT1(StatInQueue queueT1) {
        this.queueT1 = queueT1;
    }

    /**
     *
     * @return
     */
    public StatInQueue getQueueT2() {
        return queueT2;
    }

    /**
     *
     * @param queueT2
     */
    public void setQueueT2(StatInQueue queueT2) {
        this.queueT2 = queueT2;
    }

    /**
     *
     * @return
     */
    public StatInQueue getQueueRental() {
        return queueRental;
    }

    /**
     *
     * @param queueRental
     */
    public void setQueueRental(StatInQueue queueRental) {
        this.queueRental = queueRental;
    }

    /**
     *
     * @return
     */
    public StatQueue getQueueLT1() {
        return queueLT1;
    }

    /**
     *
     * @param queueLT1
     */
    public void setQueueLT1(StatQueue queueLT1) {
        this.queueLT1 = queueLT1;
    }

    /**
     *
     * @return
     */
    public StatQueue getQueueLT2() {
        return queueLT2;
    }

    /**
     *
     * @param queueLT2
     */
    public void setQueueLT2(StatQueue queueLT2) {
        this.queueLT2 = queueLT2;
    }

    /**
     *
     * @return
     */
    public StatQueue getQueueLR() {
        return queueLR;
    }

    /**
     *
     * @param queueLR
     */
    public void setQueueLR(StatQueue queueLR) {
        this.queueLR = queueLR;
    }
}
