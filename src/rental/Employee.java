/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental;

/**
 *
 * @author peter
 */
public class Employee {
    private boolean free;

    /**
     *
     */
    public Employee() {
        this.free=true;
    }

    /**
     *
     * @return
     */
    public boolean isFree() {
        return free;
    }

    /**
     *
     * @param free
     */
    public void setFree(boolean free) {
        this.free = free;
    }   
}
