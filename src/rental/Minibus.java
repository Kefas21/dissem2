/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author peter
 */
public class Minibus {
    private Queue<Customer> queue;
    private int size;

    /**
     *
     * @param size
     */
    public Minibus(int size) {
        this.queue=new LinkedList<>();
        this.size=size;
    }

    /**
     *
     * @return
     */
    public Queue<Customer> getQueue() {
        return queue;
    }

    /**
     *
     * @param queue
     */
    public void setQueue(Queue<Customer> queue) {
        this.queue = queue;
    } 

    /**
     *
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     *
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }
    
    /**
     *
     * @return
     */
    public int getFreeSize(){
        return size-queue.size();
    }
    
    /**
     *
     * @return
     */
    public boolean isFull(){
        return queue.size()==size;
    }
    
    /**
     *
     * @return
     */
    public boolean isEmpty(){
        return queue.isEmpty();
    }
}
