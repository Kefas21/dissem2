/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental;

/**
 *
 * @author peter
 */
public class Globals {

    /**
     *
     */
    public static final double T1_ARRIVAL =(60*60)/43;

    /**
     *
     */
    public static final double T2_ARRIVAL =(60*60)/19;

    /**
     *
     */
    public static final double SERVICE_MIN = 120;

    /**
     *
     */
    public static final double SERVICE_MAX = 600;

    /**
     *
     */
    public static final double CUSTOMER_INPUT_MIN = 10;

    /**
     *
     */
    public static final double CUSTOMER_INPUT_MAX = 14;

    /**
     *
     */
    public static final double CUSTOMER_OUTPUT_MIN = 4;

    /**
     *
     */
    public static final double CUSTOMER_OUTPUT_MAX = 12;

    /**
     *
     */
    public static final int BUS_CAPACITY = 12;

    /**
     *
     */
    public static final double REPLICATION_TIME = 60*60*24*30;

    /**
     *
     */
    public static final double AVERAGE_BUS_SPEED = 35/3.6;

    /**
     *
     */
    public static final double TIME_RENTAL_T1 = (6400/AVERAGE_BUS_SPEED);

    /**
     *
     */
    public static final double TIME_T1_T2 = (500/AVERAGE_BUS_SPEED);

    /**
     *
     */
    public static final double TIME_T2_RENTAL = (2500/AVERAGE_BUS_SPEED);   

    /**
     *
     */
    public static final double STUDENT_ALFA_90 = 1.645;
}
