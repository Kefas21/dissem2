/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental;

/**
 *
 * @author peter
 */
public class Customer {
    private double incomeTime;
    private double queueStartTime;
    
    /**
     *
     * @param time
     */
    public Customer(double time){
        this.incomeTime=time;
    }

    /**
     *
     * @return
     */
    public double getIncomeTime() {
        return incomeTime;
    }

    /**
     *
     * @param incomeTime
     */
    public void setIncomeTime(double incomeTime) {
        this.incomeTime = incomeTime;
    }

    /**
     *
     * @return
     */
    public double getQueueStartTime() {
        return queueStartTime;
    }

    /**
     *
     * @param queueStartTime
     */
    public void setQueueStartTime(double queueStartTime) {
        this.queueStartTime = queueStartTime;
    }

}
