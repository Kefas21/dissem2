/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rental;

/**
 *
 * @author peter
 */
public class EventMessage {
    private String time;
    private String message;
    
    /**
     *
     * @param t
     * @param m
     */
    public EventMessage(String t, String m){
        this.time=t;
        this.message=m;
    }

    /**
     *
     * @return
     */
    public String getTime() {
        return time;
    }

    /**
     *
     * @param time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     *
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
}
