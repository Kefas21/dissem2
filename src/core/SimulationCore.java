/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import java.util.PriorityQueue;

/**
 *
 * @author peter
 */
public class SimulationCore {

    private PriorityQueue<AbstractEvent> timeline;
    private double currentTime;
    private double maxTime;
    private int replications;
    private int actualRep;
    private boolean paused;
    private boolean stopped;
    private boolean start;
    private long sleepTime;

    /**
     *
     * @param max
     * @param reps
     */
    public SimulationCore(double max, int reps) {
        this.maxTime = max;
        this.replications = reps;
        this.timeline = new PriorityQueue<>();
        this.paused = false;
        this.start=false;
        this.stopped=false;
        this.sleepTime = 0;
    }
    
    /**
     *
     * @param max
     * @param reps
     * @param st
     */
    public SimulationCore(double max, int reps, long st) {
        this.maxTime = max;
        this.replications = reps;
        this.timeline = new PriorityQueue<>();
        this.paused = false;
        this.start=false;
        this.stopped=false;
        this.sleepTime = st;
    }

    /**
     *
     * @throws java.lang.InterruptedException
     */
    public void simulation() throws InterruptedException {
        this.start=true;
        beforeSimulation();
        if (!stopped){
        for (actualRep = 0; actualRep < replications; actualRep++) {
            if (stopped) break;
            beforeReplication();
            simulate();
            afterReplication();
        }
        afterSimulation();
        }
    }

    /**
     *
     */
    public void beforeSimulation() {
    }

    /**
     *
     */
    public void afterSimulation() {
    }

    /**
     *
     */
    public void beforeReplication() {
    }

    /**
     *
     * @throws InterruptedException
     */
    public void afterReplication() throws InterruptedException {
        AbstractEvent hlpEvent;
        while (!timeline.isEmpty()) {
            if (stopped) break;
            if (paused) {
                Thread.sleep(200);
            } else {
                hlpEvent = timeline.poll();
                currentTime = hlpEvent.getTime();
                hlpEvent.execute();
                Thread.sleep(sleepTime);
            }
        }
    }

    /**
     *
     * @throws InterruptedException
     */
    public final void simulate() throws InterruptedException {
        AbstractEvent hlpEvent;
        while (currentTime <= maxTime && !timeline.isEmpty()) {
            if (stopped) break;
            if (paused) {
                Thread.sleep(200);
            } else {
                hlpEvent = timeline.poll();
                currentTime = hlpEvent.getTime();
                if (hlpEvent.getTime() <= maxTime) {
                    hlpEvent.execute();
                    //System.out.println(hlpEvent.getClass());
                }
                Thread.sleep(sleepTime);
            }
        }
    }

    /**
     *
     * @param e
     */
    public void insertEvent(AbstractEvent e) {
        if (currentTime <= e.getTime()) {
            e.setCore(this);
            timeline.add(e);
        } else {
            System.out.println("Event has bigger time, not added to timeline");
        }
    }

    /**
     *
     * @return
     */
    public PriorityQueue<AbstractEvent> getTimeline() {
        return timeline;
    }

    /**
     *
     * @param timeline
     */
    public void setTimeline(PriorityQueue<AbstractEvent> timeline) {
        this.timeline = timeline;
    }

    /**
     *
     * @return
     */
    public double getCurrentTime() {
        return currentTime;
    }

    /**
     *
     * @param currentTime
     */
    public void setCurrentTime(double currentTime) {
        this.currentTime = currentTime;
    }

    /**
     *
     * @return
     */
    public double getMaxTime() {
        return maxTime;
    }

    /**
     *
     * @param maxTime
     */
    public void setMaxTime(double maxTime) {
        this.maxTime = maxTime;
    }

    /**
     *
     * @return
     */
    public int getReplications() {
        return replications;
    }

    /**
     *
     * @param replications
     */
    public void setReplications(int replications) {
        this.replications = replications;
    }

    /**
     *
     * @return
     */
    public int getActualRep() {
        return actualRep;
    }

    /**
     *
     * @param actualRep
     */
    public void setActualRep(int actualRep) {
        this.actualRep = actualRep;
    }

    /**
     *
     * @return
     */
    public boolean isPaused() {
        return paused;
    }

    /**
     *
     * @param paused
     */
    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    /**
     *
     * @return
     */
    public long getSleepTime() {
        return sleepTime;
    }

    /**
     *
     * @param sleepTime
     */
    public void setSleepTime(long sleepTime) {
        this.sleepTime = sleepTime;
    }

    /**
     *
     * @return
     */
    public boolean isStart() {
        return start;
    }

    /**
     *
     * @param start
     */
    public void setStart(boolean start) {
        this.start = start;
    }

    /**
     *
     * @return
     */
    public boolean isStopped() {
        return stopped;
    }

    /**
     *
     * @param stopped
     */
    public void setStopped(boolean stopped) {
        this.stopped = stopped;
    }

}
