/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

/**
 *
 * @author peter
 */
public abstract class AbstractEvent implements Comparable<AbstractEvent> {
    private double time;
    private SimulationCore core;
    
    /**
     *
     */
    public abstract void execute();

    /**
     *
     * @return
     */
    public double getTime() {
        return time;
    }

    /**
     *
     * @param time
     */
    public void setTime(double time) {
        this.time = time;
    }

    /**
     *
     * @return
     */
    public SimulationCore getCore() {
        return core;
    }

    /**
     *
     * @param core
     */
    public void setCore(SimulationCore core) {
        this.core = core;
    } 
    
    @Override
    public int compareTo(AbstractEvent e){
        if (this.time<e.getTime()) return -1;
        else if (this.time>e.getTime()) return 1;
        else return 0;
    }
}
