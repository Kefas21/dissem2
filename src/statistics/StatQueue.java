/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statistics;

/**
 *
 * @author peter
 */
public class StatQueue {
    private double counter;
    private double length;
    
    /**
     *
     * @param c
     * @param t
     */
    public StatQueue(double c, double t){
        this.counter=c;
        this.length=t;
    }
    
    /**
     *
     */
    public StatQueue(){
        this.counter=0;
        this.length=0;
    }
    
    /**
     *
     * @return
     */
    public double getAvgLength(){
        if (counter>0)
        return (double)length/(double)counter;
        else return 0;
    }
    
    /**
     *
     */
    public void addCounter(){
        counter++;
    }
    
    /**
     *
     * @param l
     */
    public void addLength(int l){
        this.length+=l;
    }
    
    /**
     *
     * @param l
     */
    public void addStat(int l){
        addCounter();
        addLength(l);
    }
    
    /**
     *
     */
    public void clear(){
        counter=0;
        length=0;
    }
    
}
