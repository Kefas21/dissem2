/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statistics;

import rental.Globals;

/**
 *
 * @author peter
 */
public class Stat {
    private int customerNumber;
    private double avgTime;
    private double left;
    private double right;
    private double sum;
    private double s2;
    private double s;
    private int busNumber;
    private int employeeNumber;
    private int simNr;
    
    /**
     *
     * @param c
     * @param t
     * @param s
     * @param buses
     * @param empl
     * @param snr
     */
    public Stat(int c, double t, double s, int buses, int empl,int snr){
        this.customerNumber=c;
        this.avgTime=(t/(double)customerNumber)/60;
        this.sum=s;
        this.busNumber = buses;
        this.employeeNumber = empl;
        this.simNr=snr;
        
        this.s2=sum/(double)customerNumber;
        this.s=Math.sqrt(s2);
        this.left=avgTime-((Globals.STUDENT_ALFA_90*this.s)/Math.sqrt((double)customerNumber-1))/60;
        this.right=avgTime+((Globals.STUDENT_ALFA_90*this.s)/Math.sqrt((double)customerNumber-1))/60;
    }

    /**
     *
     * @return
     */
    public int getCustomerNumber() {
        return customerNumber;
    }

    /**
     *
     * @param customerNumber
     */
    public void setCustomerNumber(int customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     *
     * @return
     */
    public double getAvgTime() {
        return avgTime;
    }

    /**
     *
     * @param avgTime
     */
    public void setAvgTime(double avgTime) {
        this.avgTime = avgTime;
    }

    /**
     *
     * @return
     */
    public double getLeft() {
        return left;
    }

    /**
     *
     * @param left
     */
    public void setLeft(double left) {
        this.left = left;
    }

    /**
     *
     * @return
     */
    public double getRight() {
        return right;
    }

    /**
     *
     * @param right
     */
    public void setRight(double right) {
        this.right = right;
    }

    /**
     *
     * @return
     */
    public double getSum() {
        return sum;
    }

    /**
     *
     * @param sum
     */
    public void setSum(double sum) {
        this.sum = sum;
    }

    /**
     *
     * @return
     */
    public double getS2() {
        return s2;
    }

    /**
     *
     * @param s2
     */
    public void setS2(double s2) {
        this.s2 = s2;
    }

    /**
     *
     * @return
     */
    public double getS() {
        return s;
    }

    /**
     *
     * @param s
     */
    public void setS(double s) {
        this.s = s;
    }

    /**
     *
     * @return
     */
    public int getBusNumber() {
        return busNumber;
    }

    /**
     *
     * @param busNumber
     */
    public void setBusNumber(int busNumber) {
        this.busNumber = busNumber;
    }

    /**
     *
     * @return
     */
    public int getEmployeeNumber() {
        return employeeNumber;
    }

    /**
     *
     * @param employeeNumber
     */
    public void setEmployeeNumber(int employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    /**
     *
     * @return
     */
    public int getSimNr() {
        return simNr;
    }

    /**
     *
     * @param simNr
     */
    public void setSimNr(int simNr) {
        this.simNr = simNr;
    }
    
}
