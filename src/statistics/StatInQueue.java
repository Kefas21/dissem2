/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statistics;

/**
 *
 * @author peter
 */
public class StatInQueue {
    private int custCounter;
    private double time;
    
    /**
     *
     * @param c
     * @param t
     */
    public StatInQueue(int c,double t){
        this.custCounter=c;
        this.time=t;
    }
    
    /**
     *
     */
    public StatInQueue(){
        this.custCounter=0;
        this.time=0;
    }
    
    /**
     *
     * @return
     */
    public double getAvgTime(){
        return time/(double)custCounter;
    }
    
    /**
     *
     */
    public void addCustomer(){
        custCounter++;
    }
    
    /**
     *
     * @param t
     */
    public void addTime(double t){
        this.time+=t;
    }
    
    /**
     *
     * @param t
     */
    public void addStat(double t){
        addCustomer();
        addTime(t);
    }
    
    /**
     *
     */
    public void clear(){
        custCounter=0;
        time=0;
    }
}
