/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import dissem2.TimeConverter;
import rental.EventMessage;
import rental.Globals;
import rental.Minibus;

/**
 *
 * @author peter
 */
public class InputStartT1 extends AbstractRentalEvent {

    private Minibus bus;

    /**
     *
     * @param b
     * @param time
     */
    public InputStartT1(Minibus b, double time) {
        this.bus = b;
        super.setTime(time);
    }

    /**
     *
     */
    @Override
    public void execute() {
        //System.out.println(super.getRentCore().getCurrentTime()+": T1: Dorazenie autobusu, volne miesto: "+bus.getFreeSize());
        if (bus.getFreeSize() > 0) {
            if (!super.getRentCore().getTerminal1().isEmpty()) {
                getRentCore().getQueueT1().addStat(getTime() - getRentCore().getTerminal1().peek().getIncomeTime());
                
                getRentCore().getQueueLT1().addStat(getRentCore().getTerminal1().size());
                super.getRentCore().insertEvent(new InputEndT1(bus, super.getRentCore().getTerminal1().poll(), super.getRentCore().getCurrentTime() + super.getRentCore().getInputGen().getNextValue()));
                getRentCore().getQueueLT1().addStat(getRentCore().getTerminal1().size());
            } else {
                super.getRentCore().insertEvent(new InputStartT2(bus, super.getRentCore().getCurrentTime() + Globals.TIME_T1_T2));
            }
        } else {
            super.getRentCore().insertEvent(new InputStartT2(bus, super.getRentCore().getCurrentTime() + Globals.TIME_T1_T2));
        }
        if (getTime() > 0 && getRentCore().getSleepTime() > 0
                && (!getRentCore().isPaused() || !getRentCore().isStopped())) {
            getRentCore().setEm(new EventMessage(TimeConverter.converter(getTime()), "Customer entries to bus at T1 with free space: " + bus.getFreeSize()));
            getRentCore().setExecTime(getTime());
            getRentCore().refreshGUI();
        }
    }

}
