/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import dissem2.TimeConverter;
import rental.EventMessage;
import rental.Globals;
import rental.Minibus;

/**
 *
 * @author peter
 */
public class InputStartT2 extends AbstractRentalEvent {

    private Minibus bus;

    /**
     *
     * @param b
     * @param time
     */
    public InputStartT2(Minibus b, double time) {
        this.bus = b;
        super.setTime(time);
    }

    /**
     *
     */
    @Override
    public void execute() {
        //System.out.println(super.getRentCore().getCurrentTime()+": T2: Dorazenie autobusu, volne miesto: "+bus.getFreeSize());
        if (bus.getFreeSize() > 0) {
            if (!super.getRentCore().getTerminal2().isEmpty()) {
                getRentCore().getQueueT2().addStat(getTime() - getRentCore().getTerminal2().peek().getIncomeTime());

                getRentCore().getQueueLT2().addStat(getRentCore().getTerminal2().size());
                super.getRentCore().insertEvent(new InputEndT2(bus, super.getRentCore().getTerminal2().poll(), super.getRentCore().getCurrentTime() + super.getRentCore().getInputGen().getNextValue()));
                getRentCore().getQueueLT2().addStat(getRentCore().getTerminal2().size());
            } else {
                super.getRentCore().insertEvent(new OutputStartRental(bus, super.getRentCore().getCurrentTime() + Globals.TIME_T2_RENTAL));
            }
        } else {
            super.getRentCore().insertEvent(new OutputStartRental(bus, super.getRentCore().getCurrentTime() + Globals.TIME_T2_RENTAL));
        }
        if (getTime() > 0 && getRentCore().getSleepTime() > 0
                && (!getRentCore().isPaused() || !getRentCore().isStopped())) {
            getRentCore().setEm(new EventMessage(TimeConverter.converter(getTime()), "Customer entries to bus at T2 with free space: " + bus.getFreeSize()));
            getRentCore().setExecTime(getTime());
            getRentCore().refreshGUI();
        }
    }

}
