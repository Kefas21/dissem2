/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import dissem2.TimeConverter;
import rental.Customer;
import rental.Employee;
import rental.EventMessage;
import rental.Minibus;

/**
 *
 * @author peter
 */
public class OutputEndRental extends AbstractRentalEvent {

    private Minibus bus;
    private Customer cust;

    /**
     *
     * @param b
     * @param c
     * @param t
     */
    public OutputEndRental(Minibus b, Customer c, double t) {
        this.bus = b;
        this.cust = c;
        super.setTime(t);
    }

    /**
     *
     */
    @Override
    public void execute() {            
        if (super.getRentCore().getRentalService().isEmpty()) {
            Employee e = super.getRentCore().employeeAvailable();
            if (e != null) {
                getRentCore().getQueueRental().addStat(0);
                super.getRentCore().insertEvent(new ServiceStart(cust, e, super.getRentCore().getCurrentTime()));
            } else {
                cust.setQueueStartTime(getTime());
                getRentCore().getQueueLR().addStat(getRentCore().getRentalService().size());
                getRentCore().getRentalService().add(cust);
                getRentCore().getQueueLR().addStat(getRentCore().getRentalService().size());
            }
        } else {
            cust.setQueueStartTime(getTime());
            getRentCore().getQueueLR().addStat(getRentCore().getRentalService().size());
            getRentCore().getRentalService().add(cust);
            getRentCore().getQueueLR().addStat(getRentCore().getRentalService().size());
        }
        if (getTime() > 0 && getRentCore().getSleepTime() > 0
                && (!getRentCore().isPaused() || !getRentCore().isStopped())) {            
            getRentCore().setEm(new EventMessage(TimeConverter.converter(getTime()), "Customer exits bus"));
            getRentCore().setExecTime(getTime());
            getRentCore().refreshGUI();
        }
        super.getRentCore().insertEvent(new OutputStartRental(bus, super.getRentCore().getCurrentTime()));
    }

}
