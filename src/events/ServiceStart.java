/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import dissem2.TimeConverter;
import rental.Customer;
import rental.Employee;
import rental.EventMessage;

/**
 *
 * @author peter
 */
public class ServiceStart extends AbstractRentalEvent {
    private Customer cust;
    private Employee emp;
    
    /**
     *
     * @param c
     * @param e
     * @param t
     */
    public ServiceStart(Customer c, Employee e, double t){
        this.cust=c;
        this.emp=e;
        super.setTime(t);
    }

    /**
     *
     */
    @Override
    public void execute() {           
        emp.setFree(false);
        
        if (getTime()>0 && getRentCore().getSleepTime()>0
                && (!getRentCore().isPaused() || !getRentCore().isStopped())) {
            getRentCore().setEm(new EventMessage(TimeConverter.converter(getTime()),"Customer starting Rental service"));
            getRentCore().setExecTime(getTime());
            getRentCore().refreshGUI();
        }
        super.getRentCore().insertEvent(new ServiceEnd(cust,emp,super.getRentCore().getCurrentTime()+super.getRentCore().getServiceGen().getNextValue()));
    }
    
}
