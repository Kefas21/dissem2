/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import core.AbstractEvent;
import rental.RentalCore;

/**
 *
 * @author peter
 */
public abstract class AbstractRentalEvent extends AbstractEvent {
    private RentalCore rentCore;
    
    /**
     *
     */
    @Override
    public abstract void execute();

    /**
     *
     * @return
     */
    public RentalCore getRentCore() {
        return rentCore;
    }

    /**
     *
     * @param rentCore
     */
    public void setRentCore(RentalCore rentCore) {
        this.rentCore = rentCore;
    }
}
