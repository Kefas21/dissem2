/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import dissem2.TimeConverter;
import rental.Customer;
import rental.EventMessage;

/**
 *
 * @author peter
 */
public class ArrivalT1 extends AbstractRentalEvent {

    private Customer cust;

    /**
     *
     * @param c
     */
    public ArrivalT1(Customer c) {
        this.cust = c;
        super.setTime(c.getIncomeTime());
    }

    /**
     *
     */
    @Override
    public void execute() {
        getRentCore().getQueueLT1().addStat(getRentCore().getTerminal1().size());
        super.getRentCore().getTerminal1().add(cust);
        getRentCore().getQueueLT1().addStat(getRentCore().getTerminal1().size());
        if (getTime() > 0 && getRentCore().getSleepTime() > 0
                && (!getRentCore().isPaused() || !getRentCore().isStopped())) {
            getRentCore().setEm(new EventMessage(TimeConverter.converter(getTime()), "New customer in Terminal 1"));
            getRentCore().setExecTime(getTime());
            getRentCore().refreshGUI();
        }
//System.out.println(super.getRentCore().getCurrentTime()+": T1: Dlzka radu: "+super.getRentCore().getTerminal1().size());
        if (super.getRentCore().getCurrentTime() < super.getRentCore().getMaxTime()) {
            super.getRentCore().insertEvent(
                    new ArrivalT1(
                            new Customer(super.getRentCore().getCurrentTime() + super.getRentCore().getTerminal1ArrivalGen().getNextValue())));
        }
    }

}
