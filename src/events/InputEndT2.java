/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import dissem2.TimeConverter;
import rental.Customer;
import rental.EventMessage;
import rental.Minibus;

/**
 *
 * @author peter
 */
public class InputEndT2 extends AbstractRentalEvent {

    private Minibus bus;
    private Customer cust;

    /**
     *
     * @param b
     * @param c
     * @param time
     */
    public InputEndT2(Minibus b, Customer c, double time) {
        this.bus=b;
        this.cust=c;
        super.setTime(time);
    }

    /**
     *
     */
    @Override
    public void execute() {
        if (getTime()>0 && getRentCore().getSleepTime()>0
                && (!getRentCore().isPaused() || !getRentCore().isStopped())) {
            getRentCore().setEm(new EventMessage(TimeConverter.converter(getTime()),"Customer is loaded to bus at T2"));
            getRentCore().setExecTime(getTime());
            getRentCore().refreshGUI();
        }
        bus.getQueue().add(cust);
        super.getRentCore().insertEvent(new InputStartT2(bus, super.getRentCore().getCurrentTime()));
    }

}
