/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import dissem2.TimeConverter;
import rental.EventMessage;
import rental.RentalCore;

/**
 *
 * @author peter
 */
public class Tick extends AbstractRentalEvent {
    
    /**
     *
     * @param t
     * @param rc
     */
    public Tick(double t, RentalCore rc){
        super.setTime(t);
        super.setRentCore(rc);
    }
    
    /**
     *
     * @param t
     */
    public Tick(double t){
        super.setTime(t);
    }

    /**
     *
     */
    @Override
    public void execute() {
        if (getRentCore().isTick()
                && getTime()>0 
                && (!getRentCore().isPaused() || !getRentCore().isStopped())) {
            getRentCore().setExecTime(getTime());
            getRentCore().setEm(new EventMessage(TimeConverter.converter(getTime()),"Tick"));
            getRentCore().refreshGUI();
        }
        System.out.println(super.getRentCore().getCurrentTime()+": TICK");
        if (super.getRentCore().getCurrentTime() < super.getRentCore().getMaxTime())
        super.getRentCore().insertEvent(new Tick(super.getRentCore().getCurrentTime()+1));
    }
    
}
