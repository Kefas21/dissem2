/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import dissem2.TimeConverter;
import rental.Customer;
import rental.Employee;
import rental.EventMessage;

/**
 *
 * @author peter
 */
public class ServiceEnd extends AbstractRentalEvent {

    private Customer cust;
    private Employee emp;

    /**
     *
     * @param c
     * @param e
     * @param t
     */
    public ServiceEnd(Customer c, Employee e, double t) {
        this.cust = c;
        this.emp = e;
        super.setTime(t);
    }

    /**
     *
     */
    @Override
    public void execute() {
        //hodit cas do statistiky
        double systemTime = super.getTime() - cust.getIncomeTime();
        super.getRentCore().setCustomerCounter(super.getRentCore().getCustomerCounter() + 1);
        super.getRentCore().setTimeCounter(super.getRentCore().getTimeCounter() + systemTime);
        double avgTime = Math.pow((super.getRentCore().getTimeCounter() / (double) super.getRentCore().getCustomerCounter()), 2);
        //super.getRentCore().setTimeCounter2(super.getRentCore().getTimeCounter2()+avgTime);
        double suma = Math.pow(systemTime, 2) - avgTime;
        super.getRentCore().setInterval(super.getRentCore().getInterval() + suma);
        //System.out.println(super.getRentCore().getCurrentTime()+": cas zakaznika v systeme: "+systemTime);

        emp.setFree(true);
        if (!super.getRentCore().getRentalService().isEmpty()) {
            getRentCore().getQueueLR().addStat(getRentCore().getRentalService().size());
            getRentCore().getQueueRental().addStat(getRentCore().getCurrentTime()-getRentCore().getRentalService().peek().getQueueStartTime());
            super.getRentCore().insertEvent(new ServiceStart(super.getRentCore().getRentalService().poll(), emp, super.getRentCore().getCurrentTime()));
            getRentCore().getQueueLR().addStat(getRentCore().getRentalService().size());
        }
        if (getTime() > 0 && getRentCore().getSleepTime() > 0
                && (!getRentCore().isPaused() || !getRentCore().isStopped())) {
            getRentCore().setEm(new EventMessage(TimeConverter.converter(getTime()), "Customer ending Rental service"));
            getRentCore().setExecTime(getTime());
            getRentCore().refreshGUI();
        }
    }

}
