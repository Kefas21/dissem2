/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import dissem2.TimeConverter;
import rental.EventMessage;
import rental.Globals;
import rental.Minibus;

/**
 *
 * @author peter
 */
public class OutputStartRental extends AbstractRentalEvent {

    private Minibus bus;

    /**
     *
     * @param b
     * @param t
     */
    public OutputStartRental(Minibus b, double t) {
        this.bus = b;
        super.setTime(t);
    }

    /**
     *
     */
    @Override
    public void execute() {
        if (getTime() > 0 && getRentCore().getSleepTime() > 0
                && (!getRentCore().isPaused() || !getRentCore().isStopped())) {
            getRentCore().setEm(new EventMessage(TimeConverter.converter(getTime()), "Customer is ready to exit bus"));
            getRentCore().setExecTime(getTime());
            getRentCore().refreshGUI();
            }
        if (!bus.isEmpty()) {
            super.getRentCore().insertEvent(new OutputEndRental(bus, bus.getQueue().poll(), super.getRentCore().getCurrentTime() + super.getRentCore().getOutputGen().getNextValue()));
        } else {
            if (super.getRentCore().getCurrentTime() < super.getRentCore().getMaxTime()) {
                super.getRentCore().insertEvent(new InputStartT1(bus, super.getRentCore().getCurrentTime() + Globals.TIME_RENTAL_T1));
            } else if (!super.getRentCore().getTerminal1().isEmpty() || !super.getRentCore().getTerminal2().isEmpty()) {
                super.getRentCore().insertEvent(new InputStartT1(bus, super.getRentCore().getCurrentTime() + Globals.TIME_RENTAL_T1));
                //System.out.println("Vyrazam z Rentalu po uplynuti casu");
            }
        }
    }

}
