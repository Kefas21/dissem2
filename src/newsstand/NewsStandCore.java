/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsstand;

import core.SimulationCore;
import generators.ExponentialGenerator;
import java.util.Queue;
import java.util.Random;

/**
 *
 * @author peter
 */
public class NewsStandCore extends SimulationCore {
    private Random seedGen;
    private ExponentialGenerator arrivalGen;
    private ExponentialGenerator serviceGen;
    private Queue<Customer> queue;
    private boolean free;

    /**
     *
     * @param max
     * @param reps
     */
    public NewsStandCore(double max, int reps) {
        super(max, reps);
    }

    /**
     *
     * @return
     */
    public Random getSeedGen() {
        return seedGen;
    }

    /**
     *
     * @param seedGen
     */
    public void setSeedGen(Random seedGen) {
        this.seedGen = seedGen;
    }

    /**
     *
     * @return
     */
    public ExponentialGenerator getArrivalGen() {
        return arrivalGen;
    }

    /**
     *
     * @param arrivalGen
     */
    public void setArrivalGen(ExponentialGenerator arrivalGen) {
        this.arrivalGen = arrivalGen;
    }

    /**
     *
     * @return
     */
    public ExponentialGenerator getServiceGen() {
        return serviceGen;
    }

    /**
     *
     * @param serviceGen
     */
    public void setServiceGen(ExponentialGenerator serviceGen) {
        this.serviceGen = serviceGen;
    }

    /**
     *
     * @return
     */
    public Queue<Customer> getQueue() {
        return queue;
    }

    /**
     *
     * @param queue
     */
    public void setQueue(Queue<Customer> queue) {
        this.queue = queue;
    }

    /**
     *
     * @return
     */
    public boolean isFree() {
        return free;
    }

    /**
     *
     * @param free
     */
    public void setFree(boolean free) {
        this.free = free;
    }
    
}
