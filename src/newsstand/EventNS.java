/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsstand;

import core.AbstractEvent;

/**
 *
 * @author peter
 */
public abstract class EventNS extends AbstractEvent {
    private Customer cust;
    private NewsStandCore coreNS;

    /**
     *
     */
    @Override
    public abstract void execute();

    /**
     *
     * @return
     */
    public Customer getCust() {
        return cust;
    }

    /**
     *
     * @param cust
     */
    public void setCust(Customer cust) {
        this.cust = cust;
    }

    /**
     *
     * @return
     */
    public NewsStandCore getCoreNS() {
        return coreNS;
    }

    /**
     *
     * @param coreNS
     */
    public void setCoreNS(NewsStandCore coreNS) {
        this.coreNS = coreNS;
    }
    
}
