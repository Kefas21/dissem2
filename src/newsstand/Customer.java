/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsstand;

/**
 *
 * @author peter
 */
public class Customer {
    private double incomeTime;
    
    /**
     *
     * @param time
     */
    public Customer(double time){
        this.incomeTime=time;
    }

    /**
     *
     * @return
     */
    public double getIncomeTime() {
        return incomeTime;
    }

    /**
     *
     * @param incomeTime
     */
    public void setIncomeTime(double incomeTime) {
        this.incomeTime = incomeTime;
    }

}
