/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsstand;

/**
 *
 * @author peter
 */
public class StartServiceEvent extends EventNS {
    
    /**
     *
     * @param c
     * @param time
     */
    public StartServiceEvent(Customer c, double time){
        super.setCust(c); 
        super.setTime(time);
    }

    /**
     *
     */
    @Override
    public void execute() {
        if (super.getCoreNS().isFree() && !super.getCoreNS().getQueue().isEmpty()){
            super.getCoreNS().insertEvent(new EndServiceEvent(super.getCoreNS().getQueue().peek(),super.getCoreNS().getCurrentTime()+super.getCoreNS().getServiceGen().getNextValue()));
            super.getCoreNS().setFree(false);
        }
    }
    
}
