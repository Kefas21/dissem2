/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsstand;

/**
 *
 * @author peter
 */
public class EndServiceEvent extends EventNS {
    
    /**
     *
     * @param c
     * @param time
     */
    public EndServiceEvent(Customer c, double time){
        super.setCust(c); 
        super.setTime(time);
    }

    /**
     *
     */
    @Override
    public void execute() {
        super.getCoreNS().getQueue().poll();
        super.getCoreNS().setFree(true);
        super.getCoreNS().insertEvent(new StartServiceEvent(super.getCoreNS().getQueue().peek(), super.getCoreNS().getCurrentTime()));
    }
    
}
