/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newsstand;

/**
 *
 * @author peter
 */
public class ArrivalEvent extends EventNS {
    
    /**
     *
     * @param c
     */
    public ArrivalEvent(Customer c){
        super.setCust(c); 
        super.setTime(c.getIncomeTime());
    }

    /**
     *
     */
    @Override
    public void execute() {
        super.getCoreNS().getQueue().add(super.getCust());
        super.getCoreNS().insertEvent(new ArrivalEvent(new Customer(super.getCoreNS().getCurrentTime() + super.getCoreNS().getArrivalGen().getNextValue())));
        super.getCoreNS().insertEvent(new StartServiceEvent(super.getCoreNS().getQueue().peek(), super.getCoreNS().getCurrentTime()));
    }
    
}
